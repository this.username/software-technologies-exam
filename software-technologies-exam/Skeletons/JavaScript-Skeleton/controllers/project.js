const Project = require('../models/Project');

module.exports = {
    index: (req, res) => {
        //TODO: Implement me ...
        Project.find().then(projects => {
            return res.render('project/index', {'projects': projects});
        }).catch(err => {
            return res.send("error");
        });
    },
    createGet: (req, res) => {
        //TODO: Implement me ...
        res.render("project/create");
    },
    createPost: (req, res) => {
        //TODO: Implement me ...
        let project = req.body;
        Project.create(project).then(() => {
            res.redirect("/");
        }).catch(err => {
            project.error = 'Cannot create project.';
            res.render('project/create', project);
        });
    },
    editGet: (req, res) => {
        //TODO: Implement me ...

        let id = req.params.id;
        Project.findById(id).then(project => {
            if (project) {
                res.render('project/edit', project );
            }
            else {
                res.redirect('/');
            }
        }).catch(err => res.redirect('/'));
    },
    editPost: (req, res) => {
        //TODO: Implement me ...
        let id = req.params.id;
        let project = req.body;

        Project.findByIdAndUpdate(id, project).then(() => {
            res.redirect("/");
        });
    },
    deleteGet: (req, res) => {
        //TODO: Implement me ...

        let id = req.params.id;
        Project.findById(id).then(project => {
            if (project) {
                res.render('project/delete', project );
            }
            else {
                res.redirect('/');
            }
        }).catch(err => res.redirect('/'));
    },
    deletePost: (req, res) => {
        //TODO: Implement me ...

        let id = req.params.id;
        let project = req.body;

        Project.findByIdAndRemove(id, project).then(() => {
            res.redirect("/");
        });
    }
};