const mongoose = require('mongoose');

let projectSchema = mongoose.Schema({
    //TODO: Implement me ...

    title: {type: 'string', required: 'true'},
    description: {type: 'string', required: 'true'},
    budget: {type: 'number', required: 'true'},
});

let Project = mongoose.model('Project', projectSchema);

module.exports = Project;