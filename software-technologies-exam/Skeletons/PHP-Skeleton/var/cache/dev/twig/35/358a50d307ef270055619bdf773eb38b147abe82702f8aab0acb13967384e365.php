<?php

/* :project:create.html.twig */
class __TwigTemplate_554ce2a565e3f40b5aec014ba1dafbebaf1d412fdc82eed325a73c6bf41a5acb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":project:create.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e79055f094aa30455b441532e2dbd18d4d5b5ca599616119bf3545c7b0f4da92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e79055f094aa30455b441532e2dbd18d4d5b5ca599616119bf3545c7b0f4da92->enter($__internal_e79055f094aa30455b441532e2dbd18d4d5b5ca599616119bf3545c7b0f4da92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":project:create.html.twig"));

        $__internal_1872595225ef59c2a935b51d8accd51ab719b3295361070e4af714929f9fcec0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1872595225ef59c2a935b51d8accd51ab719b3295361070e4af714929f9fcec0->enter($__internal_1872595225ef59c2a935b51d8accd51ab719b3295361070e4af714929f9fcec0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":project:create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e79055f094aa30455b441532e2dbd18d4d5b5ca599616119bf3545c7b0f4da92->leave($__internal_e79055f094aa30455b441532e2dbd18d4d5b5ca599616119bf3545c7b0f4da92_prof);

        
        $__internal_1872595225ef59c2a935b51d8accd51ab719b3295361070e4af714929f9fcec0->leave($__internal_1872595225ef59c2a935b51d8accd51ab719b3295361070e4af714929f9fcec0_prof);

    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        $__internal_adb373d1d643e82990812d1759fd3ac63e374f286dc0a661e4c172c21791aaf9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_adb373d1d643e82990812d1759fd3ac63e374f286dc0a661e4c172c21791aaf9->enter($__internal_adb373d1d643e82990812d1759fd3ac63e374f286dc0a661e4c172c21791aaf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_e62ddb46baff59dbafe5a727f75e7a14eb66e7b9357dfae1c8f1d3e8267be2ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e62ddb46baff59dbafe5a727f75e7a14eb66e7b9357dfae1c8f1d3e8267be2ea->enter($__internal_e62ddb46baff59dbafe5a727f75e7a14eb66e7b9357dfae1c8f1d3e8267be2ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 4
        echo "<div class=\"wrapper\">
    <form class=\"project-create\" method=\"post\">
        <div class=\"create-header\">
            Create Project
        </div>
        <div class=\"create-title\">
            <div class=\"create-title-label\">Title</div>
            <input class=\"create-title-content\" name=\"project[title]\" />
        </div>
        <div class=\"create-description\">
            <div class=\"create-description-label\">Description</div>
            <textarea rows=\"3\" class=\"create-description-content\" name=\"project[description]\"></textarea>
        </div>
        <div class=\"create-budget\">
            <div class=\"create-budget-label\">Budget</div>
            <input type=\"number\" min=\"0\" class=\"create-budget-content\" name=\"project[budget]\" />
        </div>
        <div class=\"create-button-holder\">
            <button type=\"submit\" class=\"submit-button\">Create Project</button>
            <a type=\"button\" href=\"/\" class=\"back-button\">Back</a>
        </div>

        ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
    </form>
</div>
";
        
        $__internal_e62ddb46baff59dbafe5a727f75e7a14eb66e7b9357dfae1c8f1d3e8267be2ea->leave($__internal_e62ddb46baff59dbafe5a727f75e7a14eb66e7b9357dfae1c8f1d3e8267be2ea_prof);

        
        $__internal_adb373d1d643e82990812d1759fd3ac63e374f286dc0a661e4c172c21791aaf9->leave($__internal_adb373d1d643e82990812d1759fd3ac63e374f286dc0a661e4c172c21791aaf9_prof);

    }

    public function getTemplateName()
    {
        return ":project:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 26,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block main %}
<div class=\"wrapper\">
    <form class=\"project-create\" method=\"post\">
        <div class=\"create-header\">
            Create Project
        </div>
        <div class=\"create-title\">
            <div class=\"create-title-label\">Title</div>
            <input class=\"create-title-content\" name=\"project[title]\" />
        </div>
        <div class=\"create-description\">
            <div class=\"create-description-label\">Description</div>
            <textarea rows=\"3\" class=\"create-description-content\" name=\"project[description]\"></textarea>
        </div>
        <div class=\"create-budget\">
            <div class=\"create-budget-label\">Budget</div>
            <input type=\"number\" min=\"0\" class=\"create-budget-content\" name=\"project[budget]\" />
        </div>
        <div class=\"create-button-holder\">
            <button type=\"submit\" class=\"submit-button\">Create Project</button>
            <a type=\"button\" href=\"/\" class=\"back-button\">Back</a>
        </div>

        {{ form_row(form._token) }}
    </form>
</div>
{% endblock %}", ":project:create.html.twig", "D:\\PHP-Skeleton\\app/Resources\\views/project/create.html.twig");
    }
}
