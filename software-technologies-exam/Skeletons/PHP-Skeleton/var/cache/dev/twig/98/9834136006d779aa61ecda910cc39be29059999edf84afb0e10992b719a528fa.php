<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_09886c37796d81d574b2448cf2a22ea32da8dad546b69f22c376ce19a869ce97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd81a2ec385a1b5aa0930f33085e999fa378a4b198e95f325d971bf64d2ff269 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd81a2ec385a1b5aa0930f33085e999fa378a4b198e95f325d971bf64d2ff269->enter($__internal_bd81a2ec385a1b5aa0930f33085e999fa378a4b198e95f325d971bf64d2ff269_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_35f3d52eddda7fdd842c6d1deb48eb5382fe67501f94cd42665b4767a40d19b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35f3d52eddda7fdd842c6d1deb48eb5382fe67501f94cd42665b4767a40d19b2->enter($__internal_35f3d52eddda7fdd842c6d1deb48eb5382fe67501f94cd42665b4767a40d19b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd81a2ec385a1b5aa0930f33085e999fa378a4b198e95f325d971bf64d2ff269->leave($__internal_bd81a2ec385a1b5aa0930f33085e999fa378a4b198e95f325d971bf64d2ff269_prof);

        
        $__internal_35f3d52eddda7fdd842c6d1deb48eb5382fe67501f94cd42665b4767a40d19b2->leave($__internal_35f3d52eddda7fdd842c6d1deb48eb5382fe67501f94cd42665b4767a40d19b2_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dede9265aa69493232edf1515e2fb0640085edf0a99fd610ed377da94d792fc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dede9265aa69493232edf1515e2fb0640085edf0a99fd610ed377da94d792fc7->enter($__internal_dede9265aa69493232edf1515e2fb0640085edf0a99fd610ed377da94d792fc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_fd8b5692f605b53569cd5daa9d41e4ed76c891821ca05894d5917225b58ce3b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd8b5692f605b53569cd5daa9d41e4ed76c891821ca05894d5917225b58ce3b1->enter($__internal_fd8b5692f605b53569cd5daa9d41e4ed76c891821ca05894d5917225b58ce3b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_fd8b5692f605b53569cd5daa9d41e4ed76c891821ca05894d5917225b58ce3b1->leave($__internal_fd8b5692f605b53569cd5daa9d41e4ed76c891821ca05894d5917225b58ce3b1_prof);

        
        $__internal_dede9265aa69493232edf1515e2fb0640085edf0a99fd610ed377da94d792fc7->leave($__internal_dede9265aa69493232edf1515e2fb0640085edf0a99fd610ed377da94d792fc7_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_edf35f15408d8829dbdfb915c7672b9969f26368d08d10dfbcb5eed0f99006ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edf35f15408d8829dbdfb915c7672b9969f26368d08d10dfbcb5eed0f99006ae->enter($__internal_edf35f15408d8829dbdfb915c7672b9969f26368d08d10dfbcb5eed0f99006ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e258a2a1f01d559a30d3c94378628eca8ff75738fd24506db70605b530e136c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e258a2a1f01d559a30d3c94378628eca8ff75738fd24506db70605b530e136c6->enter($__internal_e258a2a1f01d559a30d3c94378628eca8ff75738fd24506db70605b530e136c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e258a2a1f01d559a30d3c94378628eca8ff75738fd24506db70605b530e136c6->leave($__internal_e258a2a1f01d559a30d3c94378628eca8ff75738fd24506db70605b530e136c6_prof);

        
        $__internal_edf35f15408d8829dbdfb915c7672b9969f26368d08d10dfbcb5eed0f99006ae->leave($__internal_edf35f15408d8829dbdfb915c7672b9969f26368d08d10dfbcb5eed0f99006ae_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_3ce38b209e39b082a3ffcbf497f676594e189f00794140c9c267b83bdfa0d057 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ce38b209e39b082a3ffcbf497f676594e189f00794140c9c267b83bdfa0d057->enter($__internal_3ce38b209e39b082a3ffcbf497f676594e189f00794140c9c267b83bdfa0d057_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1b841d52e117a57c5651744d9adb385f754d9f15cd374de6539489dbd3806310 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b841d52e117a57c5651744d9adb385f754d9f15cd374de6539489dbd3806310->enter($__internal_1b841d52e117a57c5651744d9adb385f754d9f15cd374de6539489dbd3806310_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_1b841d52e117a57c5651744d9adb385f754d9f15cd374de6539489dbd3806310->leave($__internal_1b841d52e117a57c5651744d9adb385f754d9f15cd374de6539489dbd3806310_prof);

        
        $__internal_3ce38b209e39b082a3ffcbf497f676594e189f00794140c9c267b83bdfa0d057->leave($__internal_3ce38b209e39b082a3ffcbf497f676594e189f00794140c9c267b83bdfa0d057_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "D:\\PHP-Skeleton\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
