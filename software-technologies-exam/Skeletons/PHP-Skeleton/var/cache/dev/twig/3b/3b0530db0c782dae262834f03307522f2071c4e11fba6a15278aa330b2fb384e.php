<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_f32ad73c9bc804e7d7685ce925e27ce9c7daa98efc0b9a8c89227d7076a75563 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8df0613512d8b37bc58db8b17d0018d09af377249bafe98cbc0b88c379b9d8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8df0613512d8b37bc58db8b17d0018d09af377249bafe98cbc0b88c379b9d8b->enter($__internal_a8df0613512d8b37bc58db8b17d0018d09af377249bafe98cbc0b88c379b9d8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_849306f7313f3e266da0e79e45a306df357f021f147c78d150f2a66a8d26fcb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_849306f7313f3e266da0e79e45a306df357f021f147c78d150f2a66a8d26fcb3->enter($__internal_849306f7313f3e266da0e79e45a306df357f021f147c78d150f2a66a8d26fcb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a8df0613512d8b37bc58db8b17d0018d09af377249bafe98cbc0b88c379b9d8b->leave($__internal_a8df0613512d8b37bc58db8b17d0018d09af377249bafe98cbc0b88c379b9d8b_prof);

        
        $__internal_849306f7313f3e266da0e79e45a306df357f021f147c78d150f2a66a8d26fcb3->leave($__internal_849306f7313f3e266da0e79e45a306df357f021f147c78d150f2a66a8d26fcb3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8e5695f82448b8e013617c6f218d621ee82732873bf70d14fbd19ae0e6a9dad6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e5695f82448b8e013617c6f218d621ee82732873bf70d14fbd19ae0e6a9dad6->enter($__internal_8e5695f82448b8e013617c6f218d621ee82732873bf70d14fbd19ae0e6a9dad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_25b91e7807d15b5746d8fe7a25de64e2c3753343ff6b5e4d9ffe3d7e70a86320 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25b91e7807d15b5746d8fe7a25de64e2c3753343ff6b5e4d9ffe3d7e70a86320->enter($__internal_25b91e7807d15b5746d8fe7a25de64e2c3753343ff6b5e4d9ffe3d7e70a86320_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_25b91e7807d15b5746d8fe7a25de64e2c3753343ff6b5e4d9ffe3d7e70a86320->leave($__internal_25b91e7807d15b5746d8fe7a25de64e2c3753343ff6b5e4d9ffe3d7e70a86320_prof);

        
        $__internal_8e5695f82448b8e013617c6f218d621ee82732873bf70d14fbd19ae0e6a9dad6->leave($__internal_8e5695f82448b8e013617c6f218d621ee82732873bf70d14fbd19ae0e6a9dad6_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5935ec8e1d72728bb51a976df1b3aef6018ef52e2fec7d0aacdba369feeabd2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5935ec8e1d72728bb51a976df1b3aef6018ef52e2fec7d0aacdba369feeabd2d->enter($__internal_5935ec8e1d72728bb51a976df1b3aef6018ef52e2fec7d0aacdba369feeabd2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a6251afa2a6eb85dcd895ad5b81c0bf00b3e5a79de4ac6441d10e4cc7db9ccd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6251afa2a6eb85dcd895ad5b81c0bf00b3e5a79de4ac6441d10e4cc7db9ccd6->enter($__internal_a6251afa2a6eb85dcd895ad5b81c0bf00b3e5a79de4ac6441d10e4cc7db9ccd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_a6251afa2a6eb85dcd895ad5b81c0bf00b3e5a79de4ac6441d10e4cc7db9ccd6->leave($__internal_a6251afa2a6eb85dcd895ad5b81c0bf00b3e5a79de4ac6441d10e4cc7db9ccd6_prof);

        
        $__internal_5935ec8e1d72728bb51a976df1b3aef6018ef52e2fec7d0aacdba369feeabd2d->leave($__internal_5935ec8e1d72728bb51a976df1b3aef6018ef52e2fec7d0aacdba369feeabd2d_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_08c00cfd532c65df4cafc9cddf94bffdc4fb8d2488c8b89e423f5da8eebb5542 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08c00cfd532c65df4cafc9cddf94bffdc4fb8d2488c8b89e423f5da8eebb5542->enter($__internal_08c00cfd532c65df4cafc9cddf94bffdc4fb8d2488c8b89e423f5da8eebb5542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_78d03392ca1d32062450687fbbe52430116c0f0fafaf60751b2daa162b8313eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78d03392ca1d32062450687fbbe52430116c0f0fafaf60751b2daa162b8313eb->enter($__internal_78d03392ca1d32062450687fbbe52430116c0f0fafaf60751b2daa162b8313eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_78d03392ca1d32062450687fbbe52430116c0f0fafaf60751b2daa162b8313eb->leave($__internal_78d03392ca1d32062450687fbbe52430116c0f0fafaf60751b2daa162b8313eb_prof);

        
        $__internal_08c00cfd532c65df4cafc9cddf94bffdc4fb8d2488c8b89e423f5da8eebb5542->leave($__internal_08c00cfd532c65df4cafc9cddf94bffdc4fb8d2488c8b89e423f5da8eebb5542_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "D:\\PHP-Skeleton\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
