<?php

/* base.html.twig */
class __TwigTemplate_93e2499d03c6402ea1e3906495568257ed0120721f7f5badb0e11600e82927c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cd1b11a5cd753d7b79397a6c9f315f57cab5104dc784f443e69c743f507749e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cd1b11a5cd753d7b79397a6c9f315f57cab5104dc784f443e69c743f507749e->enter($__internal_3cd1b11a5cd753d7b79397a6c9f315f57cab5104dc784f443e69c743f507749e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_4573e43989e2777f3a5f7230b9d93d5b83db52fd36e9fc3b6c8e6147a71b855f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4573e43989e2777f3a5f7230b9d93d5b83db52fd36e9fc3b6c8e6147a71b855f->enter($__internal_4573e43989e2777f3a5f7230b9d93d5b83db52fd36e9fc3b6c8e6147a71b855f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 6
        echo "<!DOCTYPE html>
<html lang=\"en-US\">
<head>
    <meta charset=\"UTF-8\"/>
    <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>
</head>

<body id=\"";
        // line 19
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">
";
        // line 20
        $this->displayBlock('body', $context, $blocks);
        // line 23
        echo "</body>
</html>
";
        
        $__internal_3cd1b11a5cd753d7b79397a6c9f315f57cab5104dc784f443e69c743f507749e->leave($__internal_3cd1b11a5cd753d7b79397a6c9f315f57cab5104dc784f443e69c743f507749e_prof);

        
        $__internal_4573e43989e2777f3a5f7230b9d93d5b83db52fd36e9fc3b6c8e6147a71b855f->leave($__internal_4573e43989e2777f3a5f7230b9d93d5b83db52fd36e9fc3b6c8e6147a71b855f_prof);

    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        $__internal_5465e603c7c4ac563230bad56f44c69aea59f2e1e8d38a3448f62aaacecc36f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5465e603c7c4ac563230bad56f44c69aea59f2e1e8d38a3448f62aaacecc36f7->enter($__internal_5465e603c7c4ac563230bad56f44c69aea59f2e1e8d38a3448f62aaacecc36f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_453ee927c2b8f03262aa8eee59ee76f44498a25254e71697749854016181ea9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_453ee927c2b8f03262aa8eee59ee76f44498a25254e71697749854016181ea9d->enter($__internal_453ee927c2b8f03262aa8eee59ee76f44498a25254e71697749854016181ea9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Project Rider";
        
        $__internal_453ee927c2b8f03262aa8eee59ee76f44498a25254e71697749854016181ea9d->leave($__internal_453ee927c2b8f03262aa8eee59ee76f44498a25254e71697749854016181ea9d_prof);

        
        $__internal_5465e603c7c4ac563230bad56f44c69aea59f2e1e8d38a3448f62aaacecc36f7->leave($__internal_5465e603c7c4ac563230bad56f44c69aea59f2e1e8d38a3448f62aaacecc36f7_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_761c3be0b727a9e0ba3c568eef525c7ce8de1525ddd5e1b9eb5f2d84a4f6176d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_761c3be0b727a9e0ba3c568eef525c7ce8de1525ddd5e1b9eb5f2d84a4f6176d->enter($__internal_761c3be0b727a9e0ba3c568eef525c7ce8de1525ddd5e1b9eb5f2d84a4f6176d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_cc82e0e40e32c55781eea9ca971892e4a1e0102373cb14c7eb65923b0630cd67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc82e0e40e32c55781eea9ca971892e4a1e0102373cb14c7eb65923b0630cd67->enter($__internal_cc82e0e40e32c55781eea9ca971892e4a1e0102373cb14c7eb65923b0630cd67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/reset.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/create-style.css"), "html", null, true);
        echo "\">
    ";
        
        $__internal_cc82e0e40e32c55781eea9ca971892e4a1e0102373cb14c7eb65923b0630cd67->leave($__internal_cc82e0e40e32c55781eea9ca971892e4a1e0102373cb14c7eb65923b0630cd67_prof);

        
        $__internal_761c3be0b727a9e0ba3c568eef525c7ce8de1525ddd5e1b9eb5f2d84a4f6176d->leave($__internal_761c3be0b727a9e0ba3c568eef525c7ce8de1525ddd5e1b9eb5f2d84a4f6176d_prof);

    }

    // line 19
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_2224e7310b78af78be9744e50a44c5563b9fcbaec6452fb280d713b1c5ebba6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2224e7310b78af78be9744e50a44c5563b9fcbaec6452fb280d713b1c5ebba6d->enter($__internal_2224e7310b78af78be9744e50a44c5563b9fcbaec6452fb280d713b1c5ebba6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_b8d00b94a72ec00220ae5127e2154baf3432823437bda4bdbe8267e25c9cfb01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8d00b94a72ec00220ae5127e2154baf3432823437bda4bdbe8267e25c9cfb01->enter($__internal_b8d00b94a72ec00220ae5127e2154baf3432823437bda4bdbe8267e25c9cfb01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_b8d00b94a72ec00220ae5127e2154baf3432823437bda4bdbe8267e25c9cfb01->leave($__internal_b8d00b94a72ec00220ae5127e2154baf3432823437bda4bdbe8267e25c9cfb01_prof);

        
        $__internal_2224e7310b78af78be9744e50a44c5563b9fcbaec6452fb280d713b1c5ebba6d->leave($__internal_2224e7310b78af78be9744e50a44c5563b9fcbaec6452fb280d713b1c5ebba6d_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_f0c14f77a351ccc6a5e7a9a2e85ef16da506ce6fed06952d8178bda2ea284fd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0c14f77a351ccc6a5e7a9a2e85ef16da506ce6fed06952d8178bda2ea284fd5->enter($__internal_f0c14f77a351ccc6a5e7a9a2e85ef16da506ce6fed06952d8178bda2ea284fd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7e99b27917a3c31b182d3611849cf11e321fc8a3b1f1ef27efafb45890028e7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e99b27917a3c31b182d3611849cf11e321fc8a3b1f1ef27efafb45890028e7c->enter($__internal_7e99b27917a3c31b182d3611849cf11e321fc8a3b1f1ef27efafb45890028e7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "    ";
        $this->displayBlock('main', $context, $blocks);
        
        $__internal_7e99b27917a3c31b182d3611849cf11e321fc8a3b1f1ef27efafb45890028e7c->leave($__internal_7e99b27917a3c31b182d3611849cf11e321fc8a3b1f1ef27efafb45890028e7c_prof);

        
        $__internal_f0c14f77a351ccc6a5e7a9a2e85ef16da506ce6fed06952d8178bda2ea284fd5->leave($__internal_f0c14f77a351ccc6a5e7a9a2e85ef16da506ce6fed06952d8178bda2ea284fd5_prof);

    }

    public function block_main($context, array $blocks = array())
    {
        $__internal_871f14697a1bc7b0f54e8cfbbe27051e0a7bc9f2639a765b3d46d7c3f849dcdc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_871f14697a1bc7b0f54e8cfbbe27051e0a7bc9f2639a765b3d46d7c3f849dcdc->enter($__internal_871f14697a1bc7b0f54e8cfbbe27051e0a7bc9f2639a765b3d46d7c3f849dcdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_04cac332faa46f16b8aff87f1cf847695c80eb9866647039cad8c1406b909900 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04cac332faa46f16b8aff87f1cf847695c80eb9866647039cad8c1406b909900->enter($__internal_04cac332faa46f16b8aff87f1cf847695c80eb9866647039cad8c1406b909900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_04cac332faa46f16b8aff87f1cf847695c80eb9866647039cad8c1406b909900->leave($__internal_04cac332faa46f16b8aff87f1cf847695c80eb9866647039cad8c1406b909900_prof);

        
        $__internal_871f14697a1bc7b0f54e8cfbbe27051e0a7bc9f2639a765b3d46d7c3f849dcdc->leave($__internal_871f14697a1bc7b0f54e8cfbbe27051e0a7bc9f2639a765b3d46d7c3f849dcdc_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 21,  132 => 20,  115 => 19,  103 => 14,  99 => 13,  94 => 12,  85 => 11,  67 => 10,  55 => 23,  53 => 20,  49 => 19,  42 => 16,  40 => 11,  36 => 10,  30 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   This is the base template used as the application layout which contains the
   common elements and decorates all the other templates.
   See http://symfony.com/doc/current/book/templating.html#template-inheritance-and-layouts
#}
<!DOCTYPE html>
<html lang=\"en-US\">
<head>
    <meta charset=\"UTF-8\"/>
    <title>{% block title %}Project Rider{% endblock %}</title>
    {% block stylesheets %}
        <link rel=\"stylesheet\" href=\"{{ asset('css/reset.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/create-style.css') }}\">
    {% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\"/>
</head>

<body id=\"{% block body_id %}{% endblock %}\">
{% block body %}
    {% block main %}{% endblock %}
{% endblock %}
</body>
</html>
", "base.html.twig", "D:\\PHP-Skeleton\\app\\Resources\\views\\base.html.twig");
    }
}
