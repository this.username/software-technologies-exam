<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e376c751e330419d3f9a1a4b0a70adfbd1d2211ad6355062ee234a1596327f48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a17bafcef4c7b6c2df801a779ae7c0dedf52a1488f220052f39c4e9548f7d166 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a17bafcef4c7b6c2df801a779ae7c0dedf52a1488f220052f39c4e9548f7d166->enter($__internal_a17bafcef4c7b6c2df801a779ae7c0dedf52a1488f220052f39c4e9548f7d166_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_37379dd69d794001a231d827f92458d0b51bd47df8945f1500c058d77fc8b886 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37379dd69d794001a231d827f92458d0b51bd47df8945f1500c058d77fc8b886->enter($__internal_37379dd69d794001a231d827f92458d0b51bd47df8945f1500c058d77fc8b886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a17bafcef4c7b6c2df801a779ae7c0dedf52a1488f220052f39c4e9548f7d166->leave($__internal_a17bafcef4c7b6c2df801a779ae7c0dedf52a1488f220052f39c4e9548f7d166_prof);

        
        $__internal_37379dd69d794001a231d827f92458d0b51bd47df8945f1500c058d77fc8b886->leave($__internal_37379dd69d794001a231d827f92458d0b51bd47df8945f1500c058d77fc8b886_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3d505b140eafc9c8941b8663a93e30d3453528b1cffea7cafc23913b5e5d70b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d505b140eafc9c8941b8663a93e30d3453528b1cffea7cafc23913b5e5d70b2->enter($__internal_3d505b140eafc9c8941b8663a93e30d3453528b1cffea7cafc23913b5e5d70b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_279ed41c1b1f818493a2757981cd13fb46be227ad03dcc20f446ad51f0e23dc2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_279ed41c1b1f818493a2757981cd13fb46be227ad03dcc20f446ad51f0e23dc2->enter($__internal_279ed41c1b1f818493a2757981cd13fb46be227ad03dcc20f446ad51f0e23dc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_279ed41c1b1f818493a2757981cd13fb46be227ad03dcc20f446ad51f0e23dc2->leave($__internal_279ed41c1b1f818493a2757981cd13fb46be227ad03dcc20f446ad51f0e23dc2_prof);

        
        $__internal_3d505b140eafc9c8941b8663a93e30d3453528b1cffea7cafc23913b5e5d70b2->leave($__internal_3d505b140eafc9c8941b8663a93e30d3453528b1cffea7cafc23913b5e5d70b2_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_cf9d274a624d7df634b96f65aca9ff0d9e42b1b42aca01ab357026733d9905cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf9d274a624d7df634b96f65aca9ff0d9e42b1b42aca01ab357026733d9905cd->enter($__internal_cf9d274a624d7df634b96f65aca9ff0d9e42b1b42aca01ab357026733d9905cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9bce6e0e6e3f2ec0fbf6c425a5d91b3cd4e7607b06f595c9fd755e20b5bcb191 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bce6e0e6e3f2ec0fbf6c425a5d91b3cd4e7607b06f595c9fd755e20b5bcb191->enter($__internal_9bce6e0e6e3f2ec0fbf6c425a5d91b3cd4e7607b06f595c9fd755e20b5bcb191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_9bce6e0e6e3f2ec0fbf6c425a5d91b3cd4e7607b06f595c9fd755e20b5bcb191->leave($__internal_9bce6e0e6e3f2ec0fbf6c425a5d91b3cd4e7607b06f595c9fd755e20b5bcb191_prof);

        
        $__internal_cf9d274a624d7df634b96f65aca9ff0d9e42b1b42aca01ab357026733d9905cd->leave($__internal_cf9d274a624d7df634b96f65aca9ff0d9e42b1b42aca01ab357026733d9905cd_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_90f3567cf38403339a030cae4367e90cafe9606cbe1920cb20650b7af25b63f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90f3567cf38403339a030cae4367e90cafe9606cbe1920cb20650b7af25b63f8->enter($__internal_90f3567cf38403339a030cae4367e90cafe9606cbe1920cb20650b7af25b63f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0ea4e8607b501676e2452f1d31110de64f00fb8781219f9a2a180f6e68610b5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ea4e8607b501676e2452f1d31110de64f00fb8781219f9a2a180f6e68610b5a->enter($__internal_0ea4e8607b501676e2452f1d31110de64f00fb8781219f9a2a180f6e68610b5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_0ea4e8607b501676e2452f1d31110de64f00fb8781219f9a2a180f6e68610b5a->leave($__internal_0ea4e8607b501676e2452f1d31110de64f00fb8781219f9a2a180f6e68610b5a_prof);

        
        $__internal_90f3567cf38403339a030cae4367e90cafe9606cbe1920cb20650b7af25b63f8->leave($__internal_90f3567cf38403339a030cae4367e90cafe9606cbe1920cb20650b7af25b63f8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "D:\\PHP-Skeleton\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
