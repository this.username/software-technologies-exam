<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_0ef46ff3e00be850b375145c4de69a72941605b1b30816a42d1a36aa92bc080f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d45026755dcaffa8f3955da0af4075324f7878c447b2d7b7d8d413526e398b5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d45026755dcaffa8f3955da0af4075324f7878c447b2d7b7d8d413526e398b5d->enter($__internal_d45026755dcaffa8f3955da0af4075324f7878c447b2d7b7d8d413526e398b5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_ede09e8e4ac0f5e73633ad012495cf8b0b299f07808715fe4a7b85247d1ae187 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ede09e8e4ac0f5e73633ad012495cf8b0b299f07808715fe4a7b85247d1ae187->enter($__internal_ede09e8e4ac0f5e73633ad012495cf8b0b299f07808715fe4a7b85247d1ae187_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_d45026755dcaffa8f3955da0af4075324f7878c447b2d7b7d8d413526e398b5d->leave($__internal_d45026755dcaffa8f3955da0af4075324f7878c447b2d7b7d8d413526e398b5d_prof);

        
        $__internal_ede09e8e4ac0f5e73633ad012495cf8b0b299f07808715fe4a7b85247d1ae187->leave($__internal_ede09e8e4ac0f5e73633ad012495cf8b0b299f07808715fe4a7b85247d1ae187_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e381fc10ea41635d5f09d9602d8a8c4165b61ec7b2cdd0cef5c256438ec6d814 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e381fc10ea41635d5f09d9602d8a8c4165b61ec7b2cdd0cef5c256438ec6d814->enter($__internal_e381fc10ea41635d5f09d9602d8a8c4165b61ec7b2cdd0cef5c256438ec6d814_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_92b2561ce3514307a5acdde7996a9fb8f9d6b6953b7bbc92d0cdd217b591c653 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92b2561ce3514307a5acdde7996a9fb8f9d6b6953b7bbc92d0cdd217b591c653->enter($__internal_92b2561ce3514307a5acdde7996a9fb8f9d6b6953b7bbc92d0cdd217b591c653_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_92b2561ce3514307a5acdde7996a9fb8f9d6b6953b7bbc92d0cdd217b591c653->leave($__internal_92b2561ce3514307a5acdde7996a9fb8f9d6b6953b7bbc92d0cdd217b591c653_prof);

        
        $__internal_e381fc10ea41635d5f09d9602d8a8c4165b61ec7b2cdd0cef5c256438ec6d814->leave($__internal_e381fc10ea41635d5f09d9602d8a8c4165b61ec7b2cdd0cef5c256438ec6d814_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_0e677d1f571767cd922a91ace259fdf9bb0996368dcdf8436a04e102dd22e4a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e677d1f571767cd922a91ace259fdf9bb0996368dcdf8436a04e102dd22e4a5->enter($__internal_0e677d1f571767cd922a91ace259fdf9bb0996368dcdf8436a04e102dd22e4a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_c1beba92dda77f2b164df68bd1c4d6c77315144d7d0ce7ce9c558609b35fdff3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1beba92dda77f2b164df68bd1c4d6c77315144d7d0ce7ce9c558609b35fdff3->enter($__internal_c1beba92dda77f2b164df68bd1c4d6c77315144d7d0ce7ce9c558609b35fdff3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_c1beba92dda77f2b164df68bd1c4d6c77315144d7d0ce7ce9c558609b35fdff3->leave($__internal_c1beba92dda77f2b164df68bd1c4d6c77315144d7d0ce7ce9c558609b35fdff3_prof);

        
        $__internal_0e677d1f571767cd922a91ace259fdf9bb0996368dcdf8436a04e102dd22e4a5->leave($__internal_0e677d1f571767cd922a91ace259fdf9bb0996368dcdf8436a04e102dd22e4a5_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_90fbfc345557fa4af84f3bae2ce3fc7b1adb090da4cb8ca5155137498a8c5450 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90fbfc345557fa4af84f3bae2ce3fc7b1adb090da4cb8ca5155137498a8c5450->enter($__internal_90fbfc345557fa4af84f3bae2ce3fc7b1adb090da4cb8ca5155137498a8c5450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a6672225770958f1ef0a69cb513267b63129a1792e9a119cd06200812a8d0bbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6672225770958f1ef0a69cb513267b63129a1792e9a119cd06200812a8d0bbd->enter($__internal_a6672225770958f1ef0a69cb513267b63129a1792e9a119cd06200812a8d0bbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a6672225770958f1ef0a69cb513267b63129a1792e9a119cd06200812a8d0bbd->leave($__internal_a6672225770958f1ef0a69cb513267b63129a1792e9a119cd06200812a8d0bbd_prof);

        
        $__internal_90fbfc345557fa4af84f3bae2ce3fc7b1adb090da4cb8ca5155137498a8c5450->leave($__internal_90fbfc345557fa4af84f3bae2ce3fc7b1adb090da4cb8ca5155137498a8c5450_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "D:\\PHP-Skeleton\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
