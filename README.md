My exam tasks from the module Software Technologies. We were provided
with PHP, JavaScript, Java and C# skeletons, and our main task was to implement
CRUD operations. For that we had to work with different languages, frameworks,
databases and view engines. For more information, read the
Project-Rider-Description.docx file.